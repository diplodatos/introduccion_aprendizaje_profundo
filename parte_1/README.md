**Diplomatura en Ciencia de Datos, Aprendizaje Automático y sus Aplicaciones**

**Introducción al Aprendizaje profundo**

*Edición 2023*

----
# Trabajo práctico entregable - Parte 1

## Consignas
Dado el dataset que busca predecir la [presencia o ausencia de Diabetes](https://www.kaggle.com/datasets/alexteboul/diabetes-health-indicators-dataset/data?select=diabetes_binary_5050split_health_indicators_BRFSS2015.csv). Se pide:

### Ejercicio 1 - Visualización de los Datos: [Notebook 1](./ejercicio_1_visualizacion.ipynb)

Realizar una pequeña inspección del dataset que busque entender de qué se trata el dataset y el 
problema a resolver. La idea es inspeccionar no realizar un EDA completo, pueden valerse de 
herramientas que simplifiquen o realicen esta tarea, dilemas éticos, etc. Piense que los datos
pueden ser utilizados para hacer predicciones futuras.

### Ejercicio 2 - Implementar modelo básico: [Notebook 2](./ejercicio_2_baseline.ipynb)

Implementar una red neuronal simple con parámetros por defaults (este será su baseline).

### Ejercicio 3 - Implementar modelo con ajuste de hiperparámetros e interpretar resultados: [Notebook 3](./ejercicio_3_finetuning.ipynb)

En una siguiente instancia realizar una búsqueda de hiperparámetros buscando mejorar el baseline.

Mostrar e Interpretar los resultados.

### Aclaraciones importantes:

1. Pueden probar todo lo que gusten, pero deben mostrar como máximo 3 implementaciones distintas.
2. La notebook a presentar debe ser legible incluyendo:
   1. Introducción.
   2. Acompañar con comentarios que aporten a la interpretación de los resultados. 
   3. Una conclusion (breve pero no tan breve) con un resumen de lo trabajado y los resultados más
   representativos de acuerdo a su interpretación.