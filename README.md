**Diplomatura en Ciencia de Datos, Aprendizaje Automático y sus Aplicaciones**

*Edición 2023*

----
# Introducción al Aprendiaje profundo

## Getting Started

### Prerequisites

> #### Python
> - Install last version of Python and Pip.
> - Check successfully installation whit ``python --version`` command.

> #### Python Virtual Env
> - Create a new python venv with ``python -m venv .venv``.
> - After that, you will have a new folder ``.venv`` in the root project.
> - Activate venv with `` $ source .venv/bin/activate `` command in Linux and `` $ .venv/Scripts/activate `` in Windows.
> - You can check in your console if it was activated successfully.

### Installing

> #### Python Libs
> - You need install project module/libs from *requirements.txt* file.
> - `pip install -r requirements.txt`.
